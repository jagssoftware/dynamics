/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <math.h>
#include "point.h"
#include "logging.h"
#include "common.h"

#ifndef PI
#define PI 3.14159265359
#endif // PI

typedef struct Particle
{
    CartesianPoint *position; //<! position of the particle in cartesian space
    CartesianPoint *velocity; //<! velocity of the particle, in cartesian space
    double temperature;       //<! temperature (kinetic energy) of the particle
} Particle;

static CartesianPoint *randomVector3dWithModule(const double module)
{
    double theta = PI * randomUniform();
    double phi = 2. * PI * randomUniform();
    double x = module * sin(theta) * cos(phi);
    double y = module * sin(theta) * sin(phi);
    double z = module * cos(theta);
    const double coordinates[3] = {x, y, z};

    return point_new(3, coordinates);
}

Particle *particle_new(const double temperature, const double coordinates[])
{
    Particle *particle;

    particle = (Particle *)malloc(sizeof(Particle));
    particle->position = point_new(3, coordinates);
    particle->velocity = randomVector3dWithModule(sqrt(temperature));
    particle->temperature = temperature;

    return particle;
}

void particle_destroy(Particle *particle)
{
    if (particle == NULL)
    {
        logging_log("Nothing to free in here.");
        return;
    }

    point_destroy(particle->position);
    point_destroy(particle->velocity);
    free(particle);
}

CartesianPoint *particle_getPosition(const Particle *particle)
{
    return particle->position;
}

CartesianPoint *particle_getVelocity(const Particle *particle)
{
    return particle->velocity;
}

double particle_getTemperature(const Particle *particle)
{
    return particle->temperature;
}
