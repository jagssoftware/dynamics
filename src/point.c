/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "logging.h"

typedef struct CartesianPoint
{
    double *coordinates;    //!< coordinate
    unsigned int dimension; //!< dimension of the point
} CartesianPoint;

CartesianPoint *point_new(const int dimension, const double coordinates[])
{
    unsigned int i;
    CartesianPoint *point;

    point = (CartesianPoint *)malloc(sizeof(CartesianPoint));
    point->dimension = dimension;
    point->coordinates = (double *)malloc(point->dimension * sizeof(double));
    for (i = 0; i < point->dimension; i++)
    {
        point->coordinates[i] = coordinates[i];
    }

    return point;
}

void point_destroy(CartesianPoint *point)
{
    if (point == NULL)
    {
        logging_log("Nothing to free in here.");
        return;
    }

    free(point->coordinates);
    free(point);
}

void point_log(const CartesianPoint *point)
{
    logging_log("[%d](%lf, %lf, %lf)", point->dimension, point->coordinates[0], point->coordinates[1], point->coordinates[2]);
}

double point_module(const CartesianPoint *point)
{
    unsigned int i;
    double module_squared = 0.0l;

    if (point == NULL)
    {
        return 0.;
    }

    for (i = 0; i < point->dimension; i++)
    {
        module_squared += pow(point->coordinates[i], 2.);
    }

    return sqrt(module_squared);
}

CartesianPoint *point_sum(const CartesianPoint *point1, const CartesianPoint *point2)
{
    unsigned int i;
    double *coordinates;
    CartesianPoint *sum;

    if (point1 == NULL || point2 == NULL)
    {
        return NULL;
    }

    if (point1->dimension != point2->dimension)
    {
        return NULL;
    }

    coordinates = (double *)malloc(point1->dimension * sizeof(double));
    for (i = 0; i < point1->dimension; i++)
    {
        coordinates[i] = point1->coordinates[i] + point2->coordinates[i];
    }

    sum = point_new(point1->dimension, coordinates);

    free(coordinates);

    return sum;
}

CartesianPoint *point_multiplyByScalar(const double scalar, const CartesianPoint *point)
{
    unsigned int i;
    double *coordinates;
    CartesianPoint *multiplied;

    coordinates = (double *)malloc(point->dimension * sizeof(double));
    for (i = 0; i < point->dimension; i++)
    {
        coordinates[i] = scalar * point->coordinates[i];
    }

    multiplied = point_new(point->dimension, coordinates);

    free(coordinates);

    return multiplied;
}

CartesianPoint *point_negate(const CartesianPoint *point)
{
    return point_multiplyByScalar(-1., point);
}
