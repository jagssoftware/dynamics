/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __PARTICLE_H__
#define __PARTICLE_H__

#include "point.h"

/**
 * @defgroup particle Particle
 * Particle physics component.
 * @{
 */
/**
 * @brief Structure with information about a particle.
 */
typedef struct Particle Particle;

/**
 * @brief Create a particle object with temperature located in an initial position.
 * 
 * The created particle will have a velocity oriented in a random direction of 
 * the cartesian space, whose module is proportional to the temperature provided
 * as argument.
 * 
 * @param[in] temperature Temperature (Kinetic Energy) of the particle
 * @param[in] coordinates Coordinates for the position of the particle
 * @return Particle object
 */
Particle *particle_new(const double temperature, const double coordinates[]);

/**
 * @brief Destroy the particle object
 * 
 * @warning It is required to make a call to this function, after using the object,
 * in order to free the memory.
 * 
 * @param[in] particle Object to be destroyed
 */
void particle_destroy(Particle *particle);

/**
 * @brief Get the location of the particle in the space.
 * 
 * @param[in] particle Particule whose location is returned
 * @return Cartesian point with the location of the particle
 */
CartesianPoint *particle_getPosition(const Particle *particle);

/**
 * @brief Get the velocity of the particle in the velocity space.
 * 
 * @param[in] particle Particle whose velocity is returned
 * @return Cartesian point with the velocity of the particle
 */
CartesianPoint *particle_getVelocity(const Particle *particle);

/**
 * @brief Get the temperature (Kinetic Energy) of the particle
 * 
 * @param[in] particle Particle whose temperature is returned
 * @return Temperature of the particle
 */
double particle_getTemperature(const Particle *particle);

/**
 * @}
 */
#endif // __PARTICLE_H__
