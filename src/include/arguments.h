/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ARGUMENTS_H__
#define __ARGUMENTS_H__

/**
 * @defgroup arguments Arguments
 * @brief Module to extract the arguments from command line and create an object
 * to be used through the program.
 * @{
 */
/**
 * @brief Structure containing the arguments provided as arguments.
 */
typedef struct Arguments Arguments;

/**
 * @brief Read the arguments from the command line.
 * 
 * It uses the same parameters as @c main function. Be aware that the argument
 * @c argc also sums the program itself. The list of arguments in @c argv begins
 * at @c 1 and not at @c 0.
 * 
 * @param[in] argc Number of arguments
 * @param[in] argv Arguments given to the program
 * @return Object with the read arguments
 */
Arguments *arguments_read(const int argc, const char *argv[]);

/**
 * @brief Destroy the object with the arguments.
 *
 * @warning It is required after using the object to make a call to this function in
 * order to free the memory.
 *
 * @param[in] arguments Object to be destroyed.
 */
void arguments_destroy(Arguments *arguments);

/**
 * @brief Get the temperature value given as argument.
 *
 * @param[in] arguments Object with the arguments
 * @return Value of the temperature given as argument
 */
double arguments_getTemperature(const Arguments *arguments);

/**
 * @brief Get the time lapse value given as argument.
 *
 * @param[in] arguments Object with the arguments
 * @return Value of the time lapse given as argument
 */
double arguments_getTimeLapse(const Arguments *arguments);

/**
 * @}
 */
#endif // __ARGUMENTS_H__
