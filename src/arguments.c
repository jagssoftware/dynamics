/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "logging.h"

typedef struct Arguments
{
    double temperature; //<! temperature argument
    double timeLapse;   //<! time lapse
} Arguments;

Arguments *arguments_read(const int argc, const char *argv[])
{
    Arguments *arguments = NULL;
    double temperature = -1.;
    double timeLapse = -1.;

    if (argc < 3)
    {
        return NULL;
    }

    temperature = atof(argv[1]);
    timeLapse = atof(argv[2]);
    arguments = (Arguments *)malloc(sizeof(Arguments));
    arguments->temperature = temperature;
    arguments->timeLapse = timeLapse;

    return arguments;
}

void arguments_destroy(Arguments *arguments)
{
    if (arguments == NULL)
    {
        logging_log("Nothing to free here");
        return;
    }

    free(arguments);
}

double arguments_getTemperature(const Arguments *arguments)
{
    return arguments->temperature;
}

double arguments_getTimeLapse(const Arguments *arguments)
{
    return arguments->timeLapse;
}
